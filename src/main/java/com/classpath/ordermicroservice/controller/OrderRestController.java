package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
@Slf4j
public class OrderRestController {

    private final OrderService orderService;

    @GetMapping
    public Map<String, Object> fetchOrders(
            @RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo,
            @RequestParam(value = "noOfRecords", required = false, defaultValue = "10") int noOfRecords,
            @RequestParam(value = "property", required = false, defaultValue = "customerName") String property){
        log.info("Inside the fetch orders method");
        return this.orderService.fetchAllOrders(pageNo, noOfRecords, property);
    }

    @GetMapping("/{id}")
    public Order findById(@PathVariable("id") long orderId){
        return this.orderService.findOrderById(orderId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Order saveOrder(@RequestBody @Valid  Order order){
        order.getLineItems().stream().forEach(lineItem -> lineItem.setOrder(order));
        return this.orderService.save(order);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long orderId){
        this.orderService.deleteOrderById(orderId);
    }

}