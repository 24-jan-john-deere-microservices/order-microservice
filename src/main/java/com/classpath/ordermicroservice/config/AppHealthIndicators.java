package com.classpath.ordermicroservice.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;

@Configuration
class DBHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        return Health.up().withDetail("DB-Service", "DB service is up").build();
    }
}

@Configuration
class KafkaHealthIndicator implements HealthIndicator {


    @Override
    public Health health() {
        return Health.up().withDetail("Kafka", "Kafka service is up").build();
    }
}
