package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.model.LineItem;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
//@Profile({"qa", "integration-test"})
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {

    private final OrderRepository orderRepository;
    private final Faker faker = new Faker();

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        IntStream.range(1, 10_000).forEach(index -> {
            Order order = Order.builder()
                    .customerName(faker.name().firstName())
                    .date(faker.date().past( 4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                    .price(faker.number().randomDouble(2, 25_000, 45_000))
                    .email(faker.internet().emailAddress())
                    .build();
            IntStream.range(1,4).forEach(value -> {
                LineItem lineItem = LineItem.builder()
                                        .name(faker.commerce().productName())
                                        .qty(faker.number().numberBetween(2,5))
                                        .price(faker.number().randomDouble(2, 2500, 5000))
                                        .build();

               order.addLineItem(lineItem);
            });
            this.orderRepository.save(order);
        });
    }
}