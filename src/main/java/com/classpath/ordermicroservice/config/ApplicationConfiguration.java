package com.classpath.ordermicroservice.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Configuration
public class ApplicationConfiguration {

    @Bean
    // expose the rest template as fist class spring bean
    public RestTemplate restTemplate(){
        RestTemplate restTemplate =  new RestTemplate();
        restTemplate.getInterceptors().add(new LoggingClientHttpRequestInterceptor());
        return  restTemplate;

    }
}

@Slf4j
class LoggingClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        log.info(" Invoking a rest call from Order Microservice :: {}", request.getURI().toString() );
        return execution.execute(request, body);
    }
}