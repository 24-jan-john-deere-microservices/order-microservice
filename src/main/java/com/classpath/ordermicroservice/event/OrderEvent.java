package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.Order;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class OrderEvent {
    private OrderEventType eventType;
    private Order order;
    private String timestamp;
}