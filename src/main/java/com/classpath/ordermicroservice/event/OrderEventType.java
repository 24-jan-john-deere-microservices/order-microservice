package com.classpath.ordermicroservice.event;

public enum OrderEventType {
    ORDER_ACCEPTED,
    ORDER_REJECTED,
    ORDER_CANCELLED,
    ORDER_FULFILLED
}
