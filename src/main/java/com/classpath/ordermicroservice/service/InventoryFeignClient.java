package com.classpath.ordermicroservice.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

public interface InventoryFeignClient {
    @PostMapping("/api/v1/inventory")
    ResponseEntity<Integer> updateQty();
}