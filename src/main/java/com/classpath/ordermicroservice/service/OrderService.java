package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.event.OrderEvent;
import com.classpath.ordermicroservice.event.OrderEventType;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;
    private final Source source;

    public Map<String, Object> fetchAllOrders(int pageNo, int noOfRecords, String property){
        //fetch the slice of the records from the db
        Pageable pageRequest = PageRequest.of(pageNo, noOfRecords, Sort.by(property));
        Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
        long totalElements = pageResponse.getTotalElements();
        int totalPages = pageResponse.getTotalPages();
        List<Order> records = pageResponse.getContent();

        Map<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("pages", totalPages);
        responseMap.put("records", totalElements);
        responseMap.put("data", records);
        return responseMap;
    }
    public Order findOrderById(long orderId){
        return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("Invalid order id passed"));
    }

    // to update the inventory once the order is placed
    //@CircuitBreaker(name="inventoryservice" , fallbackMethod = "fallBack")
    //@Retry(name="inventoryServiceRetry" , fallbackMethod = "fallBack")
    @Transactional
    public Order save(Order order){
        log.info("About to make a call to inventory microservice:::: ");
        // call the inventory service and make a post request to update the quantity.
        //discoveryClient();
        //clientSide loadBalancer
        //loadBalancerClient();
        //use a feign client
       // inventoryFeignClient.updateQty();

        Order savedOrder = this.orderRepository.save(order);
        //publish the event
        OrderEvent orderAcceptedEvent = new OrderEvent(OrderEventType.ORDER_ACCEPTED, savedOrder, LocalDate.now().toString());
        this.source.output().send(MessageBuilder.withPayload(orderAcceptedEvent).build());
        return savedOrder;
    }

    private Order fallBack(Exception exception){
        log.error(" Exception while making the service call :: {}", exception.getMessage());
        return Order.builder().customerName("dummy").date(LocalDate.now()).build();
    }

    private void loadBalancerClient() {
        String absoluteURI = "http://inventory-service/api/v1/inventory";
        ResponseEntity<Integer> responseEntity = this.restTemplate.postForEntity(absoluteURI, null, Integer.class);
        log.info(" Response entity :: "+ responseEntity.getBody());
    }

    /*
        crude method of fetching the instance details from the discovery client and making a rest call using rest template
     */
    private void discoveryClient() {
        //construct the uri for making the rest call
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}