package com.classpath.ordermicroservice.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> handleInvalidOrderId(Exception exception){
      log.error("Exception while fetching the order for the Id :: {}" , exception.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Error(200, exception.getMessage()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Set<String>> handleInvalidPayload(MethodArgumentNotValidException exception){
        List<ObjectError> allErrors = exception.getAllErrors();
        Set<String> errorMessages = allErrors.stream().map(ObjectError::getDefaultMessage).collect(Collectors.toSet());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessages);
    }
}

@RequiredArgsConstructor
@Getter
class Error {
    private final int code;
    private final String message;
}