package com.classpath.ordermicroservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "line_items")
@NoArgsConstructor
@Setter
@Getter
@ToString(exclude = "order")
@AllArgsConstructor
@Builder
@EqualsAndHashCode(exclude = "order")
public class LineItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long itemId;

    private int qty;

    private String name;

    private double price;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    @JsonIgnore
    private Order order;
}