package com.classpath.ordermicroservice.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="orders")
@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = "lineItems")
@Builder
@AllArgsConstructor
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orderId;
    @PastOrPresent(message = "Order date cannot be in future")
    private LocalDate date;
    @Min(value = 10000, message = "Order price should be of min 10k")
    private double price;
    @NotEmpty(message = "customer name cannot be empty")
    private String customerName;
    @Email(message = "email should be a valid email address")
    private String email;
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<LineItem> lineItems;

    public void setLineItems(Set<LineItem> lineItems){
        this.lineItems = lineItems;
        lineItems.forEach(lineItem -> lineItem.setOrder(this));
    }

    //scaffolding code to set both sides of the relationship
    public void addLineItem(LineItem lineItem){
        if(this.lineItems == null){
            this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}