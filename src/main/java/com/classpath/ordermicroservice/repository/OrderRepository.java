package com.classpath.ordermicroservice.repository;

import com.classpath.ordermicroservice.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {

    List<Order> findByCustomerNameLike(String customerName);

    List<Order> findByDateBetween(LocalDate startDate, LocalDate endDate);
}